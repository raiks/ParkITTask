import React from 'react';
import TestUtils from 'react-addons-test-utils'
//import Link from '../component/photo/Photos';
import renderer from 'react-test-renderer';
import Photos from '../../component/photo/Photos.js';
import toJson from 'enzyme-to-json';

test('works', () => {
    expect(true).toBe(true)
})


it('should render a label', () => {

    const wrapper = shallow(
        <label>Hello Jest!</label>
    );
    expect(wrapper).toMatchSnapshot();
});


it('should render a photo item ', () => {
  let item = {
    media: {m:""},
		title:"",
		href:"",
		author_id:"",
		tags:""
	};

  const tree = toJson(shallow(<Photos item={item} />));
  expect(tree).toMatchSnapshot();
});