import React, { Component } from 'react';


class Photos extends Component {
  render() {
    return (

      <div className="panel panel-primary" style={{height:'300px',backgroundColor:"E0EBFA"}}>
       <div className="panel-body">

          <div className="row text-center">
            <img src={this.props.item.media.m} height="120" width="120" alt=""/>
          </div>

          <div className="row h6" style={{maxHeight: '150px', overflowY: 'scroll'}}>
            <p className="col-md-12">{this.props.item.title} by <a href={"http://flickr.com/photos/" + this.props.item.author_id}>{this.props.item.author}</a> </p>
            <p className="col-md-12"> Description: {this.props.item.title}</p>
            <p className="col-md-12"> Tags: {this.props.item.tags}</p>
          </div>
      </div>
    </div>
    );
  }
}

export default Photos;
