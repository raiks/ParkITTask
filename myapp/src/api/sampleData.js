//Testing purposes
const testdata = {
		"title": "Uploads from everyone",
		"link": "https:\/\/www.flickr.com\/photos\/",
		"description": "",
		"modified": "2017-09-15T13:20:05Z",
		"generator": "https:\/\/www.flickr.com",
		"items": 
        [
	        {
			"title": "",
			"link": "https:\/\/www.flickr.com\/photos\/146591944@N03\/36403946554\/",
			"media": {"m":"https:\/\/farm5.staticflickr.com\/4346\/36403946554_7909d0ff8b_m.jpg"},
			"date_taken": "2007-12-15T23:29:35-08:00",
			"description": " <p><a href=\"https:\/\/www.flickr.com\/people\/146591944@N03\/\">blackforest5<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/146591944@N03\/36403946554\/\" title=\"\"><img src=\"https:\/\/farm5.staticflickr.com\/4346\/36403946554_7909d0ff8b_m.jpg\" width=\"240\" height=\"160\" alt=\"\" \/><\/a><\/p> ",
			"published": "2017-09-15T13:20:05Z",
			"author": "nobody@flickr.com (\"blackforest5\")",
			"author_id": "146591944@N03",
			"tags": "graveyard grave stcuthbert parish church lothian edinburgh scotland tombstone tomb gothic moss churchyard monument castle edinburghcastle castlerock fort fortress embattlement cathedral buryingground burial grounds christian uk british inscription gravestone shadowy cemetery interment marker crypt headstone mausoleum gravedigger bury ossuary xsc"
	        },
	        {
			"title": "IMG_20170913_201132_529",
			"link": "https:\/\/www.flickr.com\/photos\/scottspy\/36403947444\/",
			"media": {"m":"https:\/\/farm5.staticflickr.com\/4404\/36403947444_d146a4cbf0_m.jpg"},
			"date_taken": "2017-09-13T18:11:32-08:00",
			"description": " <p><a href=\"https:\/\/www.flickr.com\/people\/scottspy\/\">Scottspy<\/a> posted a photo:<\/p> <p><a href=\"https:\/\/www.flickr.com\/photos\/scottspy\/36403947444\/\" title=\"IMG_20170913_201132_529\"><img src=\"https:\/\/farm5.staticflickr.com\/4404\/36403947444_d146a4cbf0_m.jpg\" width=\"240\" height=\"217\" alt=\"IMG_20170913_201132_529\" \/><\/a><\/p> ",
			"published": "2017-09-15T13:20:08Z",
			"author": "nobody@flickr.com (\"Scottspy\")",
			"author_id": "39073880@N00",
			"tags": ""
	        }
        ]
}

export default testdata