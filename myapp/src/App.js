import React, { Component } from 'react';
import Photos from './component/photo/Photos'
import request from 'superagent'
let jsonp = require('superagent-jsonp');

class App extends Component {
  constructor(props,context){
    super(props,context)
    this.state = {items:[]}
  }

componentWillMount() {
    
    var url = 'https://api.flickr.com/services/feeds/photos_public.gne?format=json'
    request
      .get(url).use(jsonp({
    timeout: 3000,
    format: 'json',
    callbackName: 'jsonFlickrFeed'}))
      .end((jsonFlickrFeed, res)=>{
        this.setState({items: res.body.items})
        })
  }    

  render() {
    return (
      <div className="container">
       <div className="row">
          <h2>Flickr Photo Stream</h2>
        </div>
        
        <div className="row">
          {this.state.items.map( (item, i) => {
            
            return (
            
                <div className="col-md-3" key={i}>
                  <Photos key={i} item={item}/>
                </div>)
          }
          
          )}
        
        </div>
      </div>
    );
  }
}

export default App;
